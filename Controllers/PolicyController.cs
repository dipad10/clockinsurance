﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Insurancedirect.Data;
using Insurancedirect.Data.Entities;
using Insurancedirect.Data.Othermodules;
using Insurancedirect.Models.Policy;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Insurancedirect.Controllers
{
    public class PolicyController : Controller
    {
        private readonly IHostingEnvironment _hostingEnvironment;
        public PolicyController(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }
        public IActionResult Motor()
        {
            MotorVM vm = new MotorVM();
            List<VehMakes> list = (new VehMakesRepository()).Getvehiclemakes();          
            ViewBag.makes = new SelectList(list, "Make", "Make");
            var years = new Mod_drive().Getlistofyears();
           
            ViewBag.years = new SelectList(years);
            return View(vm);
        }
        [HttpPost]
        public IActionResult Motor(MotorVM vm)
        {
            if(ModelState.IsValid)
            {
                //set session strings
                if(vm.Policyholdertype == "Private")
                {
                    HttpContext.Session.SetString("firstname", vm.InsuredFirstname);
                    HttpContext.Session.SetString("lastname", vm.InsuredLastname);
                    HttpContext.Session.SetString("title", vm.Title);
                }
                else
                {
                    HttpContext.Session.SetString("companyname", vm.Companyname);
                }
                HttpContext.Session.SetString("username", vm.Logininfo.Username);
                HttpContext.Session.SetString("email", vm.Logininfo.Email);
                HttpContext.Session.SetString("password", vm.Logininfo.Password);              
                HttpContext.Session.SetString("policyholdertype", vm.Policyholdertype);              
                HttpContext.Session.SetString("phone", vm.Phone);
                HttpContext.Session.SetString("identification", vm.Identification);
                HttpContext.Session.SetString("idno", vm.IdNumber);
                HttpContext.Session.SetString("occupation", vm.Occupation);
                HttpContext.Session.SetString("address", vm.Address);
                HttpContext.Session.SetString("location", vm.Location);
                HttpContext.Session.SetString("vehiclemake", vm.Motorpartialvm.VehicleMake);
                HttpContext.Session.SetString("vehiclemodel", vm.Motorpartialvm.VehicleModel);
                HttpContext.Session.SetString("vehicleyear", vm.Motorpartialvm.VehicleYear);
                HttpContext.Session.SetString("registrationnumber", vm.RegisterationNumber);
                HttpContext.Session.SetString("chasisnumber", vm.ChasisNumber);
                HttpContext.Session.SetString("colour", vm.Colour);
                HttpContext.Session.SetString("category", vm.Category);
                HttpContext.Session.SetString("usage", vm.Usage);

               return RedirectToAction("Previewinfo");

            }

            //Get all data and pass them to tempdata
            List<VehMakes> list = (new VehMakesRepository()).Getvehiclemakes();
            ViewBag.makes = new SelectList(list, "Make", "Make");
            var years = new Mod_drive().Getlistofyears();

            ViewBag.years = new SelectList(years);
            return View(vm);
        }

        public IActionResult Previewinfo()
        {
            ViewBag.policyholder = HttpContext.Session.GetString("policyholdertype");
            if(ViewBag.policyholder == "Private")
            {
                ViewBag.fullname = string.Format("{0} {1} {2}", HttpContext.Session.GetString("title"), HttpContext.Session.GetString("firstname"), HttpContext.Session.GetString("lastname"));
            }
            else
            {
                ViewBag.fullname = HttpContext.Session.GetString("companyname");
            }
            ViewBag.email = HttpContext.Session.GetString("email");
            ViewBag.username = HttpContext.Session.GetString("username");
 
            ViewBag.phone = HttpContext.Session.GetString("phone");
            ViewBag.identification = HttpContext.Session.GetString("identification");
            ViewBag.idno = HttpContext.Session.GetString("idno");
            ViewBag.occupation = HttpContext.Session.GetString("occupation");
            ViewBag.address = HttpContext.Session.GetString("address");
            ViewBag.location = HttpContext.Session.GetString("location");
            ViewBag.vehiclemake = HttpContext.Session.GetString("vehiclemake");
            ViewBag.vehiclemodel = HttpContext.Session.GetString("vehiclemodel");
            ViewBag.vehicleyear = HttpContext.Session.GetString("vehicleyear");
            ViewBag.registrationnumber = HttpContext.Session.GetString("registrationnumber");
            ViewBag.chasisnumber = HttpContext.Session.GetString("chasisnumber");
            ViewBag.colour = HttpContext.Session.GetString("colour");
            ViewBag.category = HttpContext.Session.GetString("category");
            ViewBag.usage = HttpContext.Session.GetString("usage");
            return View();
        }
        
        public string Showpath()
        {
            string result = System.IO.Path.Combine(_hostingEnvironment.WebRootPath, @"\images\home_slider_4.jpg");
            return result;
        }

        //Json AJAX GET FUNCTIONS

        public JsonResult GetvehicleMakeBrand(string vehiclemake)
        {
            List<VehMakeBrands> brands = new VehMakesBrandsRepository().GetvehiclebrandByMake(vehiclemake);
         
            return Json(new SelectList(brands, "Brand", "Brand"));
        }
    }
}