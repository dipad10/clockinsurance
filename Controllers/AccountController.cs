﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Insurancedirect.Data;
using Insurancedirect.Models.Account;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Insurancedirect.Controllers
{
    public class AccountController : Controller
    {
      
        public IActionResult Login()
        {
            LoginVM vm = new LoginVM();
            return View(vm);
        }
        [HttpPost]
        public IActionResult Login(LoginVM vm)
        {
            if (ModelState.IsValid)
            {
                //aunthenticate login
                var login = new UsersRepository().Authenticatelogin(vm);
                if (login != null)
                {
                    HttpContext.Session.SetString("username", login.Username);
                    HttpContext.Session.SetInt32("userid", login.UserId);
                    switch (login.Permission)
                    {
                        case "user":
                            return Redirect("/Home/Dashboard");
                        case "admin":
                            return Redirect("/Admin/Dashboard");

                    }
                 

                    
                }
                else
                {
                  
                    ViewBag.message = "Invalid username or password";
                    return View(vm);
                }

            }
            return View(vm);
        }

       

    }
    
}