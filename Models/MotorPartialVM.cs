﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Insurancedirect.Models
{
    public class MotorPartialVM
    {
        [Required]
        public string VehicleMake { get; set; }
     
        public string Other { get; set; }
        [Required]
        public string VehicleModel { get; set; }
        [Required]
        public string VehicleYear { get; set; }
    }
}
