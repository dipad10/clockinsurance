﻿using Insurancedirect.Data.Entities;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Insurancedirect.Models.Policy
{
    public class MotorVM
    {
        public int CertificateNo { get; set; }
        [Required]
        public string Policyholdertype { get; set; }
        public LoginInfoVM Logininfo { get; set; }
        public MotorPartialVM Motorpartialvm { get; set; }
        [Required]
        public string Title { get; set; }
        
        public string Companyname { get; set; }
        [Required]
        public string InsuredFirstname { get; set; }
        [Required]
        public string InsuredLastname { get; set; }
        [Required]
        public string Phone { get; set; }
        [Required]
        public string Identification { get; set; }
        [Required]
        public string IdNumber { get; set; }
        [Required]
        public string Occupation { get; set; }
        [Required]
        public string Address { get; set; }
        [Required]
        public string Location { get; set; }
       
        public string VehicleInsuranceClass { get; set; }
      
        [Required]
        public string RegisterationNumber { get; set; }
        [Required]
        public string EngineNumber { get; set; }
        [Required]
        public string ChasisNumber { get; set; }
        [Required]
        public string Colour { get; set; }
        [Required]
        public string Category { get; set; }
        [Required]
        public string Usage { get; set; }
        public decimal? Amount { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public string CardPinUsed { get; set; }
        public string SerialNo { get; set; }
    }
}
