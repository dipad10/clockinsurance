﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Insurancedirect.Models
{
    public class LoginInfoVM
    {
        [Required]
        public string Username { get; set; }
        [Required]
        [EmailAddress(ErrorMessage = "This is not a valid email Address")]
        public string Email { get; set; }
        [Required]
      
        public string Password { get; set; }
        [Required]
        [Compare("Password", ErrorMessage = "Please make sure the passwords match")]      
        public string Confirmpassword { get; set; }
    }
}
