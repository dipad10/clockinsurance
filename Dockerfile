#Depending on the operating system of the host machines(s) that will build or run the containers, the image specified in the FROM statement may need to be changed.
#For more information, please see https://aka.ms/containercompat


# FROM microsoft/dotnet:2.1-sdk
# WORKDIR /app

# # copy csproj and restore as distinct layers
# COPY *.csproj ./
# RUN dotnet restore

# # copy and build everything else
# COPY . ./
# RUN dotnet publish -c Release -o out
# ENTRYPOINT ["dotnet", "Insurancedirect.dll"]



# FROM microsoft/dotnet:2.1-aspnetcore-runtime AS base
# WORKDIR /app
# EXPOSE 80

# FROM microsoft/dotnet:2.1-sdk AS build
# WORKDIR /src
# COPY ["Insurancedirect/Insurancedirect.csproj", "Insurancedirect/"]
# RUN dotnet restore "Insurancedirect/Insurancedirect.csproj"
# COPY . .
# WORKDIR "/src/Insurancedirect"
# RUN dotnet build "Insurancedirect.csproj" -c Release -o /app

# FROM build AS publish
# RUN dotnet publish "Insurancedirect.csproj" -c Release -o /app

# # FROM base AS final
# # WORKDIR /app
# # COPY --from=publish /app .
# ENTRYPOINT ["dotnet", "Insurancedirect.dll"]

#FROM microsoft/dotnet:2.1-sdk
#WORKDIR /app
#
## copy csproj and restore as distinct layers
#COPY ["ClockInsurance/Insurancedirect.csproj", "ClockInsurance/"]
#RUN dotnet restore
#
## copy and build everything else
#COPY . ./
#RUN dotnet publish -c Release -o out
#ENTRYPOINT ["dotnet", "out/Insurancedirect.dll"]

FROM mcr.microsoft.com/dotnet/core/sdk:2.1 AS build-env
WORKDIR /app

# Copy csproj and restore as distinct layers
COPY Insurancedirect.csproj ./
RUN dotnet restore

# Copy everything else and build
COPY . ./
RUN dotnet publish "Insurancedirect.csproj" -c Release -o out
EXPOSE 5000
# Build runtime image
FROM mcr.microsoft.com/dotnet/core/aspnet:2.1
WORKDIR /app
COPY --from=build-env /app/out .

ENTRYPOINT ["dotnet", "Insurancedirect.dll"]