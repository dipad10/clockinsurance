﻿using Insurancedirect.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Insurancedirect.Data
{
    public class VehMakesBrandsRepository
    {
        Insurancedirect_DBContext _context = new Insurancedirect_DBContext();
        public List<VehMakeBrands> GetvehiclebrandByMake(string make)
        {

            return _context.VehMakeBrands.Where(p => p.Make == make).ToList();
        }

        public List<VehMakeBrands> Getallbrands()
        {
            return _context.VehMakeBrands.ToList();
        }


    }
}
