﻿using Insurancedirect.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Insurancedirect.Data
{
    public class VehMakesRepository
    {
        Insurancedirect_DBContext _context = new Insurancedirect_DBContext();
        public List<VehMakes> Getvehiclemakes()
        {
          
            return _context.VehMakes.ToList();
        }

        public VehMakes GetmakebyID(int makeid)
        {
            return _context.VehMakes.Where(p => p.MakeId == makeid).FirstOrDefault();
        }

      
    }
}
