﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Insurancedirect.Data.Othermodules
{
    public class Mod_drive
    {
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IConfiguration configuration;

        public string GetcategorynameByAmount(int amount)
        {
            string categoryname = "";

            switch (amount)
            {
                case 5000:
                    categoryname = "Private car";
                    break;
                case 7500:
                    categoryname = "Commercial Vehicle";
                    break;
                   
                case 2500:
                    categoryname = "Tricycle";
                    break;
                  
                case 1000:
                    categoryname = "Motorcycle";
                    break;

            }
            return categoryname;

        }
        public List<int> Getlistofyears()
        {

            return Enumerable.Range(1995, DateTime.Now.Year - 1994).OrderByDescending(i => i).ToList();
        }

        public string LastFour(string value)
        {
            if (string.IsNullOrEmpty(value) || value.Length < 3)
            {
                return "000";
            }
            return value.Substring(value.Length - 3, 3);
           }

        public string Encrypt(string clearText)
        {
            string EncryptionKey = "MAKV2SPBNI99212";
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6E, 0x20, 0x4D, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }
            return clearText;
        }


        public string Decrypt(string cipherText)
        {
            string EncryptionKey = "MAKV2SPBNI99212";
            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6E, 0x20, 0x4D, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return cipherText;
        }


        public string PopulateBody(string fullname, string Username, string Password, string Email)
        {

            string body = string.Empty;
            var result = System.IO.Path.Combine(_hostingEnvironment.WebRootPath, @"\Templates\WelcomeEmailTemplate.html");
            StreamReader reader = new StreamReader(result);
            body = reader.ReadToEnd();
            body = body.Replace("{Username}", Username);
            body = body.Replace("{Password}", Password);
            body = body.Replace("{Email}", Email);
            body = body.Replace("{fullname}", fullname);
            return body;
        }




        public bool SendHtmlFormattedEmail(string recepientEmail, string cc, string subject, string body)
        {
            try
            {
                System.Net.Mail.MailMessage mailMessage = new System.Net.Mail.MailMessage();
                mailMessage.From = new MailAddress(configuration.GetValue<string>("appsettings:UserName"), "Fastcyclepro.com");
                mailMessage.Subject = subject;
                mailMessage.Body = body;
                mailMessage.IsBodyHtml = true;
                mailMessage.To.Add(new MailAddress(recepientEmail));
                if (cc != "")
                {
                    string[] CCId = cc.Split(',');

                    foreach (string CCEmail in CCId)
                        // Adding Multiple CC email Id
                        mailMessage.CC.Add(new MailAddress(CCEmail));
                }
                else
                {
                }
                SmtpClient smtp = new SmtpClient();
                smtp.Host = configuration.GetValue<string>("appsettings:Host");
                smtp.EnableSsl = configuration.GetValue<bool>("appsettings:EnableSsl");
                System.Net.NetworkCredential NetworkCred = new System.Net.NetworkCredential();
                NetworkCred.UserName = configuration.GetValue<string>("appsettings:UserName");
                NetworkCred.Password = configuration.GetValue<string>("appsettings:Password");
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = NetworkCred;
                smtp.Port = configuration.GetValue<int>("appsettings:Port");
                smtp.Send(mailMessage);

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }


    }
}
