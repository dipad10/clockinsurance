﻿using System;
using System.Collections.Generic;

namespace Insurancedirect.Data.Entities
{
    public partial class Policies
    {
        public int CertificateNo { get; set; }
        public string PolicyNo { get; set; }
        public string Policyholdertype { get; set; }
        public string Title { get; set; }
        public int? UserId { get; set; }
        public string Companyname { get; set; }
        public string InsuredFirstname { get; set; }
        public string InsuredLastname { get; set; }
        public string Insuredname { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Identification { get; set; }
        public string IdNumber { get; set; }
        public string Occupation { get; set; }
        public string Address { get; set; }
        public string Location { get; set; }
        public string VehicleInsuranceClass { get; set; }
        public string VehicleMake { get; set; }
        public string VehicleModel { get; set; }
        public string VehicleYear { get; set; }
        public string RegisterationNumber { get; set; }
        public string EngineNumber { get; set; }
        public string ChasisNumber { get; set; }
        public string Colour { get; set; }
        public string Category { get; set; }
        public string Usage { get; set; }
        public decimal? Amount { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public string CardPinUsed { get; set; }
        public string TransGuid { get; set; }
        public string SerialNo { get; set; }
        public string Field01 { get; set; }
        public string Field02 { get; set; }
        public string Field03 { get; set; }
        public string Field04 { get; set; }
        public string Field05 { get; set; }
    }
}
