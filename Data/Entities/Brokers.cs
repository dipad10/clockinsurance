﻿using System;
using System.Collections.Generic;

namespace Insurancedirect.Data.Entities
{
    public partial class Brokers
    {
        public string BrokerId { get; set; }
        public string InsCompanyId { get; set; }
        public string BrokerName { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string MobilePhone { get; set; }
        public string ContactPerson { get; set; }
        public string Password { get; set; }
        public DateTime SubmitDate { get; set; }
        public string Rate { get; set; }
        public string Value { get; set; }
    }
}
