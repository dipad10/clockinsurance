﻿using System;
using System.Collections.Generic;

namespace Insurancedirect.Data.Entities
{
    public partial class Scratchcards
    {
        public int Serial { get; set; }
        public string PinSerial { get; set; }
        public string Batch { get; set; }
        public string Pin { get; set; }
        public string Epin { get; set; }
        public string Status { get; set; }
        public string DateUsed { get; set; }
        public string UsedBy { get; set; }
        public string Branches { get; set; }
        public string HeadMarketer { get; set; }
        public string Tag { get; set; }
        public string Remarks { get; set; }
    }
}
