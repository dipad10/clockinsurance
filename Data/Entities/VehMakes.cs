﻿using System;
using System.Collections.Generic;

namespace Insurancedirect.Data.Entities
{
    public partial class VehMakes
    {
        public long MakeId { get; set; }
        public string Make { get; set; }
        public string Tag { get; set; }
    }
}
