﻿using System;
using System.Collections.Generic;

namespace Insurancedirect.Data.Entities
{
    public partial class MarineInsuredClients
    {
        public string InsuredId { get; set; }
        public string BrokerId { get; set; }
        public string InsuredName { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string MobilePhone { get; set; }
        public string ContactPerson { get; set; }
        public string Password { get; set; }
        public DateTime SubmitDate { get; set; }
        public string Type { get; set; }
        public decimal? A1 { get; set; }
        public decimal? A2 { get; set; }
        public string Rate { get; set; }
        public string Value { get; set; }
        public string Tag { get; set; }
        public string Remarks { get; set; }
        public string Field1 { get; set; }
        public string Field2 { get; set; }
    }
}
