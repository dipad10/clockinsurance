﻿using System;
using System.Collections.Generic;

namespace Insurancedirect.Data.Entities
{
    public partial class MarineCertificates
    {
        public string CertNo { get; set; }
        public string BrokerId { get; set; }
        public string InsCompanyId { get; set; }
        public string InsuredName { get; set; }
        public DateTime TransDate { get; set; }
        public string PolicyNo { get; set; }
        public string PerDesc { get; set; }
        public string FromDesc { get; set; }
        public string ToDesc { get; set; }
        public string InterestDesc { get; set; }
        public double Rate { get; set; }
        public decimal InsuredValue { get; set; }
        public decimal GrossPrenium { get; set; }
        public string FormMno { get; set; }
        public DateTime SubmitDate { get; set; }
        public string Field1 { get; set; }
        public string Field2 { get; set; }
        public string Field3 { get; set; }
        public string Field4 { get; set; }
        public string Field5 { get; set; }
        public string Field6 { get; set; }
        public string Field7 { get; set; }
        public string Field8 { get; set; }
        public string Field9 { get; set; }
        public string Field10 { get; set; }
        public decimal? A1 { get; set; }
        public decimal? A2 { get; set; }
        public decimal? A3 { get; set; }
        public decimal? A4 { get; set; }
        public decimal? A5 { get; set; }
        public string Remarks { get; set; }
        public string Tag { get; set; }
        public string Field101 { get; set; }
        public string Field102 { get; set; }
        public string Field103 { get; set; }
        public string Field104 { get; set; }
        public string Field105 { get; set; }
        public string Field106 { get; set; }
        public string Field107 { get; set; }
        public string Field108 { get; set; }
        public string Field109 { get; set; }
    }
}
