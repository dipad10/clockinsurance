﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Insurancedirect.Data.Entities
{
    public partial class Insurancedirect_DBContext : DbContext
    {
        public Insurancedirect_DBContext()
        {
        }

        public Insurancedirect_DBContext(DbContextOptions<Insurancedirect_DBContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Brokers> Brokers { get; set; }
        public virtual DbSet<BrokersRate> BrokersRate { get; set; }
        public virtual DbSet<MarineCertificates> MarineCertificates { get; set; }
        public virtual DbSet<MarineInsuredClients> MarineInsuredClients { get; set; }
        public virtual DbSet<Policies> Policies { get; set; }
        public virtual DbSet<Scratchcards> Scratchcards { get; set; }
        public virtual DbSet<Users> Users { get; set; }
        public virtual DbSet<VehMakeBrands> VehMakeBrands { get; set; }
        public virtual DbSet<VehMakes> VehMakes { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Data Source=DIPAD;Initial Catalog=Insurancedirect_DB;User ID=sa;Password=gibs321.");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Brokers>(entity =>
            {
                entity.HasKey(e => e.BrokerId);

                entity.Property(e => e.BrokerId)
                    .HasColumnName("BrokerID")
                    .HasMaxLength(16)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Address)
                    .IsRequired()
                    .HasMaxLength(128)
                    .IsUnicode(false);

                entity.Property(e => e.BrokerName)
                    .IsRequired()
                    .HasMaxLength(64)
                    .IsUnicode(false);

                entity.Property(e => e.ContactPerson)
                    .IsRequired()
                    .HasMaxLength(128)
                    .IsUnicode(false);

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(128)
                    .IsUnicode(false);

                entity.Property(e => e.InsCompanyId)
                    .IsRequired()
                    .HasColumnName("InsCompanyID")
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.MobilePhone)
                    .IsRequired()
                    .HasMaxLength(128)
                    .IsUnicode(false);

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Rate)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.SubmitDate).HasColumnType("datetime");

                entity.Property(e => e.Value)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<BrokersRate>(entity =>
            {
                entity.HasKey(e => e.RateId);

                entity.Property(e => e.RateId).HasColumnName("RateID");

                entity.Property(e => e.BrokerId)
                    .IsRequired()
                    .HasColumnName("BrokerID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.BuSource)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.BusClass)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Field1)
                    .HasColumnName("field1")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Field2)
                    .HasColumnName("field2")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Field3)
                    .HasColumnName("field3")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Field4)
                    .HasColumnName("field4")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Field5)
                    .HasColumnName("field5")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Rate).HasColumnType("decimal(18, 4)");

                entity.Property(e => e.Remarks)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Subclass)
                    .HasColumnName("subclass")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SubclassRate)
                    .HasColumnName("subclass_rate")
                    .HasColumnType("decimal(18, 4)");

                entity.Property(e => e.Tag)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<MarineCertificates>(entity =>
            {
                entity.HasKey(e => e.CertNo);

                entity.ToTable("Marine_Certificates");

                entity.Property(e => e.CertNo)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.BrokerId)
                    .IsRequired()
                    .HasColumnName("BrokerID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Field1)
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.Field10)
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.Field101)
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.Field102)
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.Field103)
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.Field104)
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.Field105)
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.Field106)
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.Field107)
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.Field108)
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.Field109)
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.Field2)
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.Field3)
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.Field4)
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.Field5)
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.Field6)
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.Field7)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Field8)
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.Field9)
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.FormMno)
                    .IsRequired()
                    .HasColumnName("FormMNo")
                    .HasMaxLength(1028)
                    .IsUnicode(false);

                entity.Property(e => e.FromDesc)
                    .IsRequired()
                    .HasMaxLength(1028)
                    .IsUnicode(false);

                entity.Property(e => e.GrossPrenium).HasColumnType("money");

                entity.Property(e => e.InsCompanyId)
                    .IsRequired()
                    .HasColumnName("InsCompanyID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.InsuredName)
                    .IsRequired()
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.InsuredValue).HasColumnType("money");

                entity.Property(e => e.InterestDesc)
                    .IsRequired()
                    .HasMaxLength(1028)
                    .IsUnicode(false);

                entity.Property(e => e.PerDesc)
                    .IsRequired()
                    .HasMaxLength(1028)
                    .IsUnicode(false);

                entity.Property(e => e.PolicyNo)
                    .IsRequired()
                    .HasMaxLength(128)
                    .IsUnicode(false);

                entity.Property(e => e.Remarks)
                    .HasMaxLength(1028)
                    .IsUnicode(false);

                entity.Property(e => e.SubmitDate).HasColumnType("datetime");

                entity.Property(e => e.Tag)
                    .HasMaxLength(1028)
                    .IsUnicode(false);

                entity.Property(e => e.ToDesc)
                    .IsRequired()
                    .HasMaxLength(1028)
                    .IsUnicode(false);

                entity.Property(e => e.TransDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<MarineInsuredClients>(entity =>
            {
                entity.HasKey(e => e.InsuredId);

                entity.ToTable("Marine_InsuredClients");

                entity.Property(e => e.InsuredId)
                    .HasColumnName("InsuredID")
                    .HasMaxLength(16)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Address)
                    .IsRequired()
                    .HasMaxLength(128)
                    .IsUnicode(false);

                entity.Property(e => e.BrokerId)
                    .IsRequired()
                    .HasColumnName("BrokerID")
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.ContactPerson)
                    .IsRequired()
                    .HasMaxLength(128)
                    .IsUnicode(false);

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(128)
                    .IsUnicode(false);

                entity.Property(e => e.Field1)
                    .HasMaxLength(128)
                    .IsUnicode(false);

                entity.Property(e => e.Field2)
                    .HasMaxLength(128)
                    .IsUnicode(false);

                entity.Property(e => e.InsuredName)
                    .IsRequired()
                    .HasMaxLength(64)
                    .IsUnicode(false);

                entity.Property(e => e.MobilePhone)
                    .IsRequired()
                    .HasMaxLength(128)
                    .IsUnicode(false);

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Rate)
                    .HasMaxLength(128)
                    .IsUnicode(false);

                entity.Property(e => e.Remarks)
                    .HasMaxLength(128)
                    .IsUnicode(false);

                entity.Property(e => e.SubmitDate).HasColumnType("datetime");

                entity.Property(e => e.Tag)
                    .HasMaxLength(128)
                    .IsUnicode(false);

                entity.Property(e => e.Type)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Value)
                    .HasMaxLength(128)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Policies>(entity =>
            {
                entity.HasKey(e => e.CertificateNo);

                entity.Property(e => e.Address)
                    .HasMaxLength(400)
                    .IsUnicode(false);

                entity.Property(e => e.Amount).HasColumnType("money");

                entity.Property(e => e.CardPinUsed)
                    .HasColumnName("Card_pin_used")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Category)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ChasisNumber)
                    .HasColumnName("Chasis_number")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Colour)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Companyname)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EffectiveDate)
                    .HasColumnName("Effective_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Email)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.EngineNumber)
                    .HasColumnName("Engine_number")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ExpiryDate)
                    .HasColumnName("Expiry_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Field01)
                    .HasColumnName("field01")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Field02)
                    .HasColumnName("field02")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Field03)
                    .HasColumnName("field03")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Field04)
                    .HasColumnName("field04")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Field05)
                    .HasColumnName("field05")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IdNumber)
                    .HasColumnName("Id_number")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Identification)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.InsuredFirstname)
                    .HasColumnName("Insured_firstname")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.InsuredLastname)
                    .HasColumnName("Insured_lastname")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Insuredname)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Location)
                    .HasColumnName("location")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Occupation)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Phone)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PolicyNo)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Policyholdertype)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RegisterationNumber)
                    .HasColumnName("Registeration_number")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SerialNo)
                    .HasColumnName("serial_no")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Title)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TransGuid)
                    .HasColumnName("TransGUID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Usage)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UserId).HasColumnName("UserID");

                entity.Property(e => e.VehicleInsuranceClass)
                    .HasColumnName("Vehicle_Insurance_class")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.VehicleMake)
                    .HasColumnName("Vehicle_make")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.VehicleModel)
                    .HasColumnName("Vehicle_model")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.VehicleYear)
                    .HasColumnName("Vehicle_year")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Scratchcards>(entity =>
            {
                entity.HasKey(e => e.Serial);

                entity.ToTable("scratchcards");

                entity.Property(e => e.Serial).HasColumnName("serial");

                entity.Property(e => e.Batch)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Branches)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.DateUsed)
                    .HasColumnName("Date_used")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Epin)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HeadMarketer)
                    .HasColumnName("Head_marketer")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Pin)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PinSerial)
                    .HasColumnName("Pin_Serial")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Remarks)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Tag)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UsedBy)
                    .HasColumnName("Used_by")
                    .HasMaxLength(200)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Users>(entity =>
            {
                entity.HasKey(e => e.UserId);

                entity.Property(e => e.UserId).HasColumnName("UserID");

                entity.Property(e => e.Address)
                    .HasMaxLength(400)
                    .IsUnicode(false);

                entity.Property(e => e.Email)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Field01)
                    .HasColumnName("field01")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Field02)
                    .HasColumnName("field02")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Field03)
                    .HasColumnName("field03")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Field04)
                    .HasColumnName("field04")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Field05)
                    .HasColumnName("field05")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IdNumber)
                    .HasColumnName("Id_number")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Identification)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.InsuredName)
                    .HasColumnName("Insured_name")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Location)
                    .HasColumnName("location")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Occupation)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Password)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Permission)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Phone)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Title)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Username)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<VehMakeBrands>(entity =>
            {
                entity.HasKey(e => e.BrandId);

                entity.Property(e => e.BrandId).HasColumnName("BrandID");

                entity.Property(e => e.Brand)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.FullName)
                    .HasMaxLength(3000)
                    .IsUnicode(false);

                entity.Property(e => e.Make)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.MakeId).HasColumnName("MakeID");
            });

            modelBuilder.Entity<VehMakes>(entity =>
            {
                entity.HasKey(e => e.MakeId);

                entity.Property(e => e.MakeId).HasColumnName("MakeID");

                entity.Property(e => e.Make)
                    .HasColumnName("make")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Tag)
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });
        }
    }
}
