﻿using System;
using System.Collections.Generic;

namespace Insurancedirect.Data.Entities
{
    public partial class Users
    {
        public int UserId { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Title { get; set; }
        public string InsuredName { get; set; }
        public string Location { get; set; }
        public string Identification { get; set; }
        public string IdNumber { get; set; }
        public string Phone { get; set; }
        public string Occupation { get; set; }
        public string Address { get; set; }
        public string Permission { get; set; }
        public string Field01 { get; set; }
        public string Field02 { get; set; }
        public string Field03 { get; set; }
        public string Field04 { get; set; }
        public string Field05 { get; set; }
    }
}
