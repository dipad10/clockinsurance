﻿using System;
using System.Collections.Generic;

namespace Insurancedirect.Data.Entities
{
    public partial class BrokersRate
    {
        public long RateId { get; set; }
        public string BrokerId { get; set; }
        public string BusClass { get; set; }
        public string BuSource { get; set; }
        public decimal Rate { get; set; }
        public string Remarks { get; set; }
        public string Tag { get; set; }
        public string Subclass { get; set; }
        public decimal? SubclassRate { get; set; }
        public string Field1 { get; set; }
        public string Field2 { get; set; }
        public string Field3 { get; set; }
        public string Field4 { get; set; }
        public string Field5 { get; set; }
    }
}
