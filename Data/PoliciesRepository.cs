﻿using Insurancedirect.Data.Entities;
using Insurancedirect.Models.Policy;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Insurancedirect.Data
{
    public class PoliciesRepository
    {
        Insurancedirect_DBContext _context = new Insurancedirect_DBContext();

        public IEnumerable<Policies> GetPolicies()
        {
            return _context.Policies.OrderByDescending(p => p.CertificateNo).ToList();
        }

        public IEnumerable<Policies> Getuserpolicies(int userid)
        {
            return _context.Policies.Where(p => p.UserId == userid).ToList();
        }
        public Policies Getpolicybycertno(int certno)
        {
            return _context.Policies.Where(p => p.CertificateNo == certno).FirstOrDefault();
        }

        public int Insertpolicy(MotorVM vm)
        {
            Policies pol = new Policies();
            if(vm.Policyholdertype == "Private")
            {
                pol.InsuredFirstname = vm.InsuredFirstname;
                pol.InsuredLastname = vm.InsuredLastname;
            }
            else { pol.Companyname = vm.Companyname; }

            _context.Policies.Add(pol);
            return _context.SaveChanges();

        }

        public void Makesessions(MotorVM vm)
        {
          
        }
    }
}
