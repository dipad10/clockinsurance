﻿using Insurancedirect.Data.Entities;
using Insurancedirect.Models.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Insurancedirect.Data
{
    public class UsersRepository
    {
        Insurancedirect_DBContext _context = new Insurancedirect_DBContext();
        public IEnumerable<Users> GetUsers()
        {
            return _context.Users.OrderByDescending(p => p.UserId).ToList();
        }

        public Users GetuserbyID(int userid)
        {
            return _context.Users.Where(p => p.UserId == userid).FirstOrDefault();
        }

        public Users Authenticatelogin(LoginVM login)
        {
            return _context.Users.Where(p => p.Email == login.Email && p.Password == login.Password).FirstOrDefault();

        }

    }
}
